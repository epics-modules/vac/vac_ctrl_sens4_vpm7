/*asynInterposeThrottle.c */
/***********************************************************************/

/* Interpose for devices where there must be a delay between writes
 */

#include <sys/errno.h>
#include <sys/time.h>
#include <time.h>

#include <cantProceed.h>
#include <epicsStdio.h>
#include <epicsString.h>
#include <iocsh.h>

#include <epicsExport.h>
#include "asynDriver.h"
#include "asynOctet.h"
#include <epicsExport.h>


typedef struct interposePvt {
    asynInterface  octet;
    asynOctet      *pasynOctetDrv;
    void           *drvPvt;
    /* the delay between writes */
    struct timeval delay;
    /* the earliest possible write */
    struct timeval next_write;
}interposePvt;
    
/* asynOctet methods */
static asynStatus writeIt(void *ppvt, asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);
static asynStatus readIt(void *ppvt, asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);
static asynStatus flushIt(void *ppvt, asynUser *pasynUser); 
static asynStatus registerInterruptUser(void *drvPvt, asynUser *pasynUser, interruptCallbackOctet callback, void *userPvt, void **registrarPvt);
static asynStatus cancelInterruptUser(void *drvPvt, asynUser *pasynUser, void *registrarPvt);
static asynStatus setInputEos(void *ppvt, asynUser *pasynUser, const char *eos, int eoslen);
static asynStatus getInputEos(void *ppvt, asynUser *pasynUser, char *eos, int eossize, int *eoslen);
static asynStatus setOutputEos(void *ppvt, asynUser *pasynUser, const char *eos, int eoslen);
static asynStatus getOutputEos(void *ppvt, asynUser *pasynUser, char *eos, int eossize, int *eoslen);


static asynOctet octet = {
    	writeIt, 
	readIt, 
	flushIt,
    	registerInterruptUser, 
	cancelInterruptUser,
    	setInputEos, 
	getInputEos, 
	setOutputEos, 
	getOutputEos
};


static void tv_now(struct timeval *now)
{
    struct timespec spec_now;
    clock_gettime(CLOCK_MONOTONIC_RAW, &spec_now);
    now->tv_sec = spec_now.tv_sec;
    now->tv_usec = spec_now.tv_nsec / 1000;
}


epicsShareFunc int 
asynInterposeThrottle(const char *portName, int addr, int delay)
{
    interposePvt *pvt;
    asynStatus status;
    asynInterface *poctetasynInterface;

    pvt = callocMustSucceed(1, sizeof(interposePvt), "asynInterposeThrottle");
    pvt->octet.interfaceType = asynOctetType;
    pvt->octet.pinterface = &octet;
    pvt->octet.drvPvt = pvt;

    status = pasynManager->interposeInterface(portName, addr, &pvt->octet, &poctetasynInterface);
    if ((status!=asynSuccess) || !poctetasynInterface)
    {
	printf("%s interposeInterface failed.\n", portName);
        free(pvt);
        return -1;
    }
    pvt->pasynOctetDrv = (asynOctet *)poctetasynInterface->pinterface;
    pvt->drvPvt = poctetasynInterface->drvPvt;

    /* Initialize the timer structures */
    pvt->delay.tv_usec = delay * 1000;
    struct timeval now;
    tv_now(&now);
    timeradd(&pvt->delay, &now, &pvt->next_write);

    return 0;
}


/* asynOctet methods */
static asynStatus writeIt(void *ppvt, asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)
{
    interposePvt *pvt = (interposePvt *)ppvt;
    struct timeval now;
    struct timeval delay;

    /* Calculate how much time to wait */
    tv_now(&now);
    timersub(&pvt->next_write, &now, &delay);
    while (select(0, NULL, NULL, NULL, &delay) < 0 && errno == EINTR) ;

    asynStatus status = pvt->pasynOctetDrv->write(pvt->drvPvt, pasynUser, data, numchars, nbytesTransfered);

    /* Update next possible write time */
    tv_now(&now);
    timeradd(&now, &pvt->delay, &pvt->next_write);

    return status;
}

static asynStatus readIt(void *ppvt, asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->read(pvt->drvPvt, pasynUser, data, maxchars, nbytesTransfered, eomReason);
}

static asynStatus flushIt(void *ppvt, asynUser *pasynUser)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->flush(pvt->drvPvt, pasynUser);
}

static asynStatus registerInterruptUser(void *ppvt, asynUser *pasynUser, interruptCallbackOctet callback, void *userPvt, void **registrarPvt)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->registerInterruptUser( pvt->drvPvt, pasynUser, callback, userPvt, registrarPvt);
}

static asynStatus cancelInterruptUser(void *drvPvt, asynUser *pasynUser, void *registrarPvt)
{
    interposePvt *pvt = (interposePvt *)drvPvt;

    return pvt->pasynOctetDrv->cancelInterruptUser( pvt->drvPvt, pasynUser, registrarPvt);
}

static asynStatus setInputEos(void *ppvt, asynUser *pasynUser, const char *eos, int eoslen)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->setInputEos(pvt->drvPvt, pasynUser, eos, eoslen);
}

static asynStatus getInputEos(void *ppvt, asynUser *pasynUser, char *eos, int eossize, int *eoslen)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->getInputEos(pvt->drvPvt, pasynUser, eos, eossize, eoslen);
}

static asynStatus setOutputEos(void *ppvt, asynUser *pasynUser, const char *eos, int eoslen)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->setOutputEos(pvt->drvPvt, pasynUser, eos, eoslen);
}

static asynStatus getOutputEos(void *ppvt, asynUser *pasynUser, char *eos, int eossize, int *eoslen)
{
    interposePvt *pvt = (interposePvt *)ppvt;

    return pvt->pasynOctetDrv->getOutputEos(pvt->drvPvt, pasynUser, eos, eossize, eoslen);
}


/* register asynInterposeThrottle*/
static const iocshArg asynInterposeThrottleArg0 = { "portName", iocshArgString };
static const iocshArg asynInterposeThrottleArg1 = { "addr", iocshArgInt };
static const iocshArg asynInterposeThrottleArg2 = { "delay", iocshArgInt };
static const iocshArg *asynInterposeThrottleArgs[] = {&asynInterposeThrottleArg0, &asynInterposeThrottleArg1, &asynInterposeThrottleArg2};
static const iocshFuncDef asynInterposeThrottleFuncDef = {"asynInterposeThrottle", 3, asynInterposeThrottleArgs};

static void asynInterposeThrottleCallFunc(const iocshArgBuf *args)
{
    asynInterposeThrottle(args[0].sval, args[1].ival, args[2].ival);
}

static void asynInterposeThrottleRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        firstTime = 0;
        iocshRegister(&asynInterposeThrottleFuncDef, asynInterposeThrottleCallFunc);
    }
}
epicsExportRegistrar(asynInterposeThrottleRegister);
