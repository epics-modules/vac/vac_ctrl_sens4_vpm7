# Vacuum pressure transducer Sens4 VPM-7 SmartPirani ATM

EPICS module to provide communications and read/write data from/to Sens4 VPM-7 SmartPirani ATM vacuum pressure transducer

## MOXA configuration

#### Operation Modes / Operating settings

```
Application: Socket
Mode: TCP Server
```

OR

```
Operation mode: TCP Server mode
```

```
...
Packing length: 0
Delimiter 1: 00 / Disabled
Delimiter 2: 00 / Disabled
Delimiter process: Do Nothing
Force transmit: 0
```

#### Communication Parameters / Serial Settings

The default baudrate is 9600 but recommend changing it to 115200. Of course the MOXA and device settings have to match, so you have to change it first on the device and then on the MOXA.

````
Baud rate: 115200
Data bits: 8
Stop bits: 1
Parity: None
Flow control: None
Interface: RS-485 2-wire
```

